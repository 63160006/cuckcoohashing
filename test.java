public class test {
    public static void main(String[] args) {
        CuckcooHashing cuckoo = new CuckcooHashing();
        cuckoo.put(98, "Thanwa");
        cuckoo.put(13, "Saensud");
        cuckoo.put(157, "Thuch");
        cuckoo.put(173, "Somchai");
        System.out.println(cuckoo.get(98).toString());
        System.out.println(cuckoo.get(13).toString());
        System.out.println(cuckoo.get(157).toString());
        System.out.println(cuckoo.get(173).toString());
        cuckoo.delete(173);
        System.out.println(cuckoo.get(173));
    }
}
