import java.util.Arrays;

public class CuckcooHashing {
    private int key;
    private String value;
    private CuckcooHashing[] cuckcoo1 = new CuckcooHashing[89];
    private CuckcooHashing[] cuckcoo2 = new CuckcooHashing[89];
    
    public CuckcooHashing(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public CuckcooHashing() {
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public int hashMap1(int key) {
        return key%cuckcoo1.length;
    }

    public int hashMap2(int key) {
        return (key/89)%cuckcoo2.length;
    }

    public CuckcooHashing get(int key) {
        if(cuckcoo1[hashMap1(key)]!=null&&cuckcoo1[hashMap1(key)].key==key) {
            return cuckcoo1[hashMap1(key)];
        }
        if(cuckcoo2[hashMap2(key)]!=null&&cuckcoo2[hashMap2(key)].key==key) {
            return cuckcoo2[hashMap2(key)];
        }
        return null;
    }

    public void put(int key, String value) {
        if (cuckcoo1[hashMap1(key)] != null && cuckcoo1[hashMap1(key)].key == key) {
            cuckcoo1[hashMap1(key)].key = key;
            cuckcoo1[hashMap1(key)].value = value;
            return;
        }
        if (cuckcoo2[hashMap2(key)] != null && cuckcoo2[hashMap2(key)].key == key) {
            cuckcoo2[hashMap2(key)].key = key;
            cuckcoo2[hashMap2(key)].value = value;
            return;
        }
        int i = 0;

        while (true) {
            if (i == 0) {
                if (cuckcoo1[hashMap1(key)] == null) {
                    cuckcoo1[hashMap1(key)] = new CuckcooHashing();
                    cuckcoo1[hashMap1(key)].key = key;
                    cuckcoo1[hashMap1(key)].value = value;
                    return;
                }
            } else {
                if (cuckcoo2[hashMap2(key)] == null) {
                    cuckcoo2[hashMap2(key)] = new CuckcooHashing();
                    cuckcoo2[hashMap2(key)].key = key;
                    cuckcoo2[hashMap2(key)].value = value;
                    return;
                }
            }
            if (i == 0) {
                String tempValue = cuckcoo1[hashMap1(key)].value;
                int tempKey = cuckcoo1[hashMap1(key)].key;
                cuckcoo1[hashMap1(key)].key = key;
                cuckcoo1[hashMap1(key)].value = value;
                key = tempKey;
                value = tempValue;
            } else {
                String tempValue = cuckcoo2[hashMap2(key)].value;
                int tempKey = cuckcoo2[hashMap2(key)].key;
                cuckcoo2[hashMap2(key)].key = key;
                cuckcoo2[hashMap2(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }

    public CuckcooHashing delete(int key) {
        if (cuckcoo1[hashMap1(key)] != null && cuckcoo1[hashMap1(key)].key == key) {
            CuckcooHashing temp = cuckcoo1[hashMap1(key)];
            cuckcoo1[hashMap1(key)] = null;
            return temp;
        }
        if (cuckcoo2[hashMap2(key)] != null && cuckcoo2[hashMap2(key)].key == key) {
            CuckcooHashing temp = cuckcoo2[hashMap2(key)];
            cuckcoo2[hashMap2(key)] = null;
            return temp;
        }
        return null;
    }

    @Override
    public String toString() {
        return "key : " + key + "value : " + value;
    }
}
